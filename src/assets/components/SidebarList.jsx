import {
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from "@mui/material";
import React from "react";

const SidebarList = ({ icon, primaryText, secondaryText }) => {
  return (
    <ListItem disablePadding sx={{ marginBottom: 1 }}>
      <ListItemButton>
        <ListItemIcon>{icon}</ListItemIcon>
        <ListItemText primary={primaryText} secondary={secondaryText} />
      </ListItemButton>
    </ListItem>
  );
};

export default SidebarList;
