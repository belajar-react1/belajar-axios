import { Box } from "@mui/material";
import React from "react";

const Sidebar = ({ children }) => {
  return (
    <>
      <Box
        sx={{
          display: {
            xs: "none",
            sm: "none",
            md: "flex",
          },
          flexDirection: "column",
          width: 400,
          height: "100vh",
          boxShadow: 1,
          padding: 5,
        }}
      >
        {children}
      </Box>
    </>
  );
};

export default Sidebar;
